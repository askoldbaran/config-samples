package com.example.configclientsecond;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigClientSecondApplication {

    public static void main(String[] args) {

        SpringApplication.run(ConfigClientSecondApplication.class, args);
    }

}
